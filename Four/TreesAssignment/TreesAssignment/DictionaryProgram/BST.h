#ifndef BST_H
#define BST_H

using namespace std;

class BST {

	//Private Member Vars
	private:

		//Tree Node Struct
		struct Leaf {
			char* data;
			Leaf* left;
			Leaf* right;	
			Leaf* parent;
			int leftHeight;
			int rightHeight;
			int leafHeight;
		};

		Leaf* treeRoot;
		void UpdateHeight(Leaf* root, int& currHeight);
		void printTree(Leaf* p, int &currNum);

	//Public Methods
	public:

		//Constructs
		BST(void);
		~BST(void);
		
		//Functions
		void buildAVLTree(char** newData, int wordCount);
		Leaf* MakeLeaf(char* newData, Leaf* currParent);
		void InsertLeaf(char* newData);
		void UpdateHeight(Leaf* root);		
		void rotateRight(Leaf* currLeaf);
		void rotateLeft(Leaf* currLeaf);
		void printTree();
		void findMin(Leaf* root);
		bool isInTree(char* newData);
		
};

#endif