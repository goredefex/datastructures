
#ifndef ARRAYCONTROLS_H
#define ARRAYCONTROLS_H

#include <string>
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <sstream>

using namespace std;

class ArrayControls {

	//Constant Values
	const int size;
	const int bottom;

	//Private Member Vars
	private:
		//Member Vars
		int* currArray;

		//Methods
		int* deleteArray(int* ptr);
		int randomize(int start, int end);
		void fillBlankArray();
						
	//Public Methods
	public:
		//Constructs
		ArrayControls(void);
		~ArrayControls(void);

		//Getters
		string getArrayString(int* arrayToConvert, int currSize);
		int* getCurrArray();
		int getSize();

		//Functions
		void printArray(int* arrayToPrint, int currSize);
		void printArray(string* arrayToPrint, int lineCount);
		string convertInt(int newNum);
		char** MakeCharWordArray(string* currArr, int wordCount);
		void displayALine(string newMsg);
		void displayMsg(string newMsg);
		void unsortTheArray();
		void reverseTheArray();

};

#endif