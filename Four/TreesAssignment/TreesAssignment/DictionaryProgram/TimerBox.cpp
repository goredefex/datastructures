#include "stdafx.h"
#include "TimerBox.h"

//==============================================================
//Constructs ---------------------------------------------------
//==============================================================

TimerBox::TimerBox(void) {
	this->timeVariant = CLOCKS_PER_SEC;
	this->timePassed = 0.0;
	//this->setStart(this->variate(clock()));

} //end constructor

TimerBox::TimerBox(bool miliseconds) {
	//Have Not Finished Minutes Timer
	this->timeVariant = 1;
	this->timePassed = 0.0;
	//this->setStart(this->variate(clock()));

} //end constructor

//==============================================================




//==============================================================
//Getters ------------------------------------------------------
//==============================================================

//Returns 'start' Member Var
clock_t TimerBox::getStart() { return this->start; } //end function

//Returns 'end' Member Var
clock_t TimerBox::getEnd() { return this->end; } //end function

//Returns 'timePassed' Member Var
double TimerBox::getTimePassed() { return this->timePassed; } //end function

//==============================================================




//==============================================================
//Setters ------------------------------------------------------
//==============================================================

//Takes clock_t Type & Sets 'start' Member Var
void TimerBox::setStart(clock_t newTime) { this->start = newTime; } //end function

//Takes clock_t Type & Sets 'end' Member Var
void TimerBox::setEnd(clock_t newTime) { this->end = newTime; } //end function

//Takes double Type & Sets 'timePassed' Member Var
void TimerBox::setTimePassed(double newTime) { this->timePassed = newTime; } //end function

//==============================================================




//==============================================================
//Functions ----------------------------------------------------
//==============================================================

clock_t TimerBox::variate(clock_t currTime) {
	return currTime/timeVariant;

} //end function

void TimerBox::checkTimePassed() {
	this->setTimePassed(difftime(this->getEnd(), this->getStart()));

} //end function

void TimerBox::stopTimer() {
	this->setEnd(this->variate(clock()));
	this->checkTimePassed();

} //end function

void TimerBox::startTimer() {
	this->setStart(this->variate(clock()));

} //end function

void TimerBox::addSecond() { this->end += 1000; } //end function

//==============================================================


//==============================================================
//Friends ------------------------------------------------------
//==============================================================

ostream& operator<<(ostream& stream, TimerBox& timer) {
	//Convert Double For Printing
	stream << to_string(timer.getTimePassed()); //<--No Line Term
	
	return stream;

} //end friend