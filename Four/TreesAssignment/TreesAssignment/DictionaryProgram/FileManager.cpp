
#include "stdafx.h"
#include "FileManager.h"

//==============================================================
//Constructs ---------------------------------------------------
//==============================================================

//Constructor --------------------------------------------------
FileManager::FileManager(string newFile) {
	this->setFileName(newFile);
	this->setRowsReadIn(0);
	this->setColsReadIn(0);
	
} //end constructor

//Default For Pre-Instances
FileManager::FileManager(void) {} //end constructor

//Destructor --------------------------------------------------
FileManager::~FileManager(void) {
	this->data = deleteStringArray(this->data);

} //end destructor

//==============================================================




//==============================================================
//Getters ------------------------------------------------------
//==============================================================

//Returns 'fileName' Member Var
string FileManager::getFileName() { return this->fileName; } //end function

//Returns 'contents' Member Var
string FileManager::getContents() { return this->contents; } //end function

//Returns Word Array Member Var
string* FileManager::getWordArray() { return this->wordArray; } //end function

//Returns 'charAmmount' Member Var
int FileManager::getCharAmmount() { return this->charAmmount; } //end function

//Returns 'data' Member Var
char* FileManager::getCharData() { return this->data; } //end function

//Returns 'rowsReadIn' Member Var
int FileManager::getRowsReadIn() { return this->rowsReadIn; } //end function

//Returns 'colsReadIn' Member Var
int FileManager::getColsReadIn() { return this->colsReadIn; } //end function

//==============================================================




//==============================================================
//Setters ------------------------------------------------------
//==============================================================

//Sets 'filename' Member Var
void FileManager::setFileName(string newFile) { this->fileName = "c:/"+newFile; } //end function

//Sets 'contents' Member Var
void FileManager::setContents(string newContents) { this->contents = newContents; } //end function

void FileManager::setWordArray(string* newArr) { this->wordArray = newArr; } //end function

//Sets 'charAmmount' Member Var
void FileManager::setCharAmmount(int newAmmount) { this->charAmmount = newAmmount; } //end function

//Sets 'rowsReadIn' Member Var
void FileManager::setRowsReadIn(int newAmmount) { this->rowsReadIn = newAmmount; } //end function

//Sets 'colsReadIn' Member Var
void FileManager::setColsReadIn(int newAmmount) { this->colsReadIn = newAmmount; } //end function

//==============================================================



//==============================================================
//Functions ----------------------------------------------------
//==============================================================

//Deletes and 'Safe's' The Given Pointer
char* FileManager::deleteStringArray(char* ptr) {
	
	if(ptr!=NULL) {
		delete(ptr);
		ptr = NULL;

	} 

	return ptr;

} //end function

//Takes An int Array Pointer &
//Returns A String Of Numbers
string FileManager::convertIntArray(int* intArray, int size) {
	stringstream conversion;
	for (int i=0; i<size; i++) {
		conversion << intArray[i] << endl;
	}

    return conversion.str();

} //end function


//Takes string & Adds It To 'contents' Member Var
void FileManager::addToContents(string newContents) { this->contents += newContents; } //end function

//Takes string & Adds It To 'contents' Member Var As It's Own Line
void FileManager::addLineToContents(string newContents) { this->contents += newContents + "\n"; } //end function

//Enforces That 'contents' Member Var string Type Is Cleared
void FileManager::wipeContents() { this->contents.clear(); } //end function

//Enforces The 'data' Member Var Points To 'contents' Member Var Array
void FileManager::convertContentsToData() { this->data = &this->contents[0]; } //end function

//Using 'rowsReadIn' Member Var & Increases Its Value By 1
void FileManager::increaseRowsRead() { this->rowsReadIn++; } //end function

//Using 'colsReadIn' Member Var & Increases Its Value By 1
void FileManager::increaseColsRead() { this->colsReadIn++; } //end function


//Turns The Contents Member Var Into Char** Array Of Words
void FileManager::tokenizeContentsStringToWordArray() {

	//Array Controls
	this->wordArray = new string[250];
	int counter = 0;

	//Function Controls
	size_t start = 0, trailCounter = this->contents.find_first_of(' ');
	bool found;
	//Safety
	if(trailCounter!=0) { found = true; }
	else { trailCounter == string::npos; }

	//Search Loop
	while(trailCounter!=string::npos) {

		if(found) {
			if(start!=0) { start++; }
			string token = this->contents.substr(start, (trailCounter-start));
			this->wordArray[counter] = token;
			//cout << this->wordArray[counter];
			counter++;
			//cout << counter << endl;

			if(this->contents[trailCounter+1]==' ') {
				found = false;
				trailCounter++;

			} else { 
				found = true;
				start = trailCounter;
				if(trailCounter != string::npos) {
					trailCounter = this->contents.find_first_of(" ", start+1);
				}
			}

		} else {
			if(this->contents[trailCounter+1]==' ') {
				found = false;
				trailCounter++;
			
			} else { 
				found = true;
				start = trailCounter;
				if(trailCounter != string::npos) {
					trailCounter = this->contents.find_first_of(" ", start+1);
				}
			}
		}
	
	}

	this->rowsReadIn = counter;

} //end function

//Incase Of Different OS Change Return Char To Universal '\n' Char
void FileManager::changeReturnChars() {

	size_t currCharToReplace = string::npos;

	do {
		currCharToReplace = this->contents.find_first_of("\r");
		if(currCharToReplace != string::npos) {
			this->contents[currCharToReplace] = '\n';
		}
	
	} while(currCharToReplace!=string::npos);

} //end function

//Get Rid Of All '\n' Chars
void FileManager::removeLineTerminators() {
	
	size_t currCharToRemove = string::npos;

	do {
		string charToFind = "\n";
		currCharToRemove = this->contents.find_first_of(charToFind);
		
		if(currCharToRemove != string::npos) {
			this->contents[currCharToRemove] = ' ';
		}
	
	} while(currCharToRemove!=string::npos);

} //end function

//
void FileManager::removeSpecificChar(string newCharToRemove) {
	
	size_t currCharToRemove = string::npos;

	do {
		currCharToRemove = this->contents.find_first_of(newCharToRemove);
		
		if(currCharToRemove != string::npos) {
			this->contents[currCharToRemove] = ' ';
		}
	
	} while(currCharToRemove!=string::npos);

} //end function

//Counts & Sets The 'rowsReadIn' & 'colsReadIn' Member Vars
void FileManager::countDimensions() {
	
	size_t searchChar = string::npos;
	size_t trailChar = 0;
	bool foundColLength = false;

	do {
		searchChar = this->contents.find_first_of("\n", trailChar);
		this->increaseRowsRead();
		if(!foundColLength) {
			this->setColsReadIn(searchChar);
			foundColLength = true;
		}
		trailChar = searchChar + 1;

	} while(searchChar!=string::npos);

} //end function

//Counts & Finds Total Character Amount In Given File
int FileManager::countFileSize() {
	
	//Total Initiated
	int counter = 0;

	ifstream inFile; 
	inFile.exceptions(ios::badbit);

	try {

		inFile.open(this->getFileName(), std::ifstream::binary); 
		
		if(inFile.fail()) {
			cout << "Cannot Read Filename. Please Re-enter..." << endl;
			throw "Cannot Read File For Counting";

		} else {

			while (inFile.good()) {
				inFile.get(); 
				counter++;

			}

			//Protect Against Breaks
			try {
				inFile.close();
			} catch(std::ios_base::failure& e) {
				cout << "Could Not Close Size Counter" << endl;
			}

		}
		
	} catch(string err) {
		
		cout << err << endl;
	}
	
	//Update Char Totals
	this->setCharAmmount(counter);

	return counter;

} //end function

//Reads Given Text File In To 'contents' Member Var
bool FileManager::readFile() {
	
	ifstream inFile; 
	inFile.exceptions(std::ios::badbit | std::ios::failbit);
	bool valid = false;

	try {

		inFile.open(this->getFileName(), std::ifstream::binary); 

		if(!inFile.fail()) {
			
			while (inFile.good()) {
				string token;
				getline(inFile, token); 
				this->addToContents(token);
				this->rowsReadIn++;
			}

			//Protect Against Breaks
			try {
				inFile.close();
				valid = true;
			} catch(std::ios_base::failure& e) {
				cout << "Could Not Close Reader" << endl;
			}

		}

	} catch(std::exception const& err) {
		
		//cout << err.what() << "Could Not Open File To Read!" << endl;
		valid = false;
	}

	
	//Char Chars For Reading
	this->changeReturnChars();

	//Update Row/Col Totals
	this->countDimensions();
	
	return valid;

} //end function

bool FileManager::readNoCounting() {

	ifstream inFile; 
	inFile.exceptions(std::ios::badbit | std::ios::failbit);
	bool valid = false;

	try {

		inFile.open(this->getFileName(), std::ifstream::binary); 

		if(!inFile.fail()) {
			
			while (inFile.good()) {
				string token;
				getline(inFile, token); 
				this->addToContents(token);
				this->rowsReadIn++;
			}

			//Protect Against Breaks
			try {
				inFile.close();
				valid = true;
			} catch(std::ios_base::failure& e) {
				cout << "Could Not Close Reader" << endl;
			}

		}

	} catch(std::exception const& err) {
		
		//cout << err.what() << "Could Not Open File To Read!" << endl;
		valid = false;
	}

	
	//Char Chars For Reading
	this->changeReturnChars();

	return valid;

} //end function

//Reads Given Text File In To 'contents' Member Var
bool FileManager::readFileIntoStringArray() {
	
	ifstream inFile; 
	inFile.exceptions(std::ios::badbit | std::ios::failbit);
	bool valid = false;
	this->readFile();
	this->wordArray = new string[this->rowsReadIn];
	
	try {

		inFile.open(this->getFileName(), std::ifstream::binary); 

		if(!inFile.fail()) {
			int counter = 0;
			while (inFile.good()) {
				string token;
				getline(inFile, token); 
				this->wordArray[counter] = token;
				counter++;
			}

			//Protect Against Breaks
			try {
				inFile.close();
				valid = true;
			} catch(std::ios_base::failure& e) {
				cout << "Could Not Close Reader" << endl;
			}

		}

	} catch(std::exception const& err) {
		
		//cout << err.what() << "Could Not Open File To Read!" << endl;
		valid = false;
	}
			
	return valid;

} //end function

//Takes A Name To Be Given To New Test File 
//Generated From Method. Uses 'contents' Member Var
//To Send As Text To Generated Text File.
void FileManager::generateFile(string newFileName) {

	try {

		ofstream newFile("c:/"+newFileName);
		newFile.exceptions(std::ios::badbit);

		if(!newFile.fail()) {
			newFile << this->getContents();

		}

	} catch(...) {
		cout << "Cannot Generate File" << endl;

	}

} //end function
