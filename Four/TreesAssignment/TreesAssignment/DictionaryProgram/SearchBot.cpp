#include "stdafx.h"
#include "SearchBot.h"

//==============================================================
//Constructs ---------------------------------------------------
//==============================================================

//Constructor
SearchBot::SearchBot(void) { //<--Default
	
	this->lowerBound = 0;
	this->upperBound = 0;

} //end constructor

SearchBot::SearchBot(int newLower, int newUpper) { //<--Default
	
	this->lowerBound = newLower;
	this->upperBound = newUpper;

} //end constructor


//Destructor
SearchBot::~SearchBot(void) {} //end destructor

//==============================================================





//==============================================================
//Setters ------------------------------------------------------
//==============================================================

//Sets The 'lowerBound' Member Var To Given int newNum
void SearchBot::setLowerBound(int newNum) { this->lowerBound = newNum; } //end function

//Sets The 'lowerBound' Member Var To Given int newNum
void SearchBot::setUpperBound(int newNum) { this->upperBound = newNum; } //end function

//==============================================================





//==============================================================
//Getters ------------------------------------------------------
//==============================================================

//Returns The 'lowerBound' Member Var
int SearchBot::getLowerBound() { return this->lowerBound; } //end function

//Returns The 'upperBound' Member Var
int SearchBot::getUpperBound() { return this->upperBound; } //end function

//==============================================================





//==============================================================
//Functions ----------------------------------------------------
//==============================================================

//Finds Given numToFind Within currArray Given & Returns Index Found
//If -1 If Returned Then The Number Was Not In The Array
int SearchBot::sequentialSearch(int* currArray, int size, int numToFind) {
	int found = -1;
	for(int i=0; i<size; i++) {
		if(currArray[i] == numToFind) { found=i; break; }
	}

	return found;

} //end function

//Uses A Non-Recursive Implementation To Return Index Of numToFind
int SearchBot::binarySearchNonRecursive(int* currArray, int size, int numToFind) {
	int found = -1;
	int root = -1;
	this->lowerBound = 0;
	this->upperBound = size;

	while(this->lowerBound+1 < this->upperBound) {
		root = (this->lowerBound + this->upperBound)/2;
		if(currArray[root]>numToFind) {
			this->upperBound = root;

		} else {
			this->lowerBound = root;
		
		}	
	}

	//If Last Number Filtered Is numToFind
	if(currArray[this->lowerBound] == numToFind) { found = this->lowerBound; }

	return found;

} //end function


//Uses Recursion To Find
int SearchBot::binarySearch(int* currArray, int size, int numToFind) {
	
	return 0;

} //end function

//==============================================================