#include "stdafx.h"
#include "ArrayControls.h"


//==============================================================
//Constructs ---------------------------------------------------
//==============================================================

//Constructor
ArrayControls::ArrayControls(void):size(10000), bottom(0) {

	srand(time(0));
	this->currArray = new int[size];
	this->fillBlankArray();
		
} //end constructor

//Destructor
ArrayControls::~ArrayControls(void) {
	this->currArray = this->deleteArray(this->currArray);
} //end destructor

//==============================================================





//==============================================================
//Getters ------------------------------------------------------
//==============================================================

string ArrayControls::getArrayString(int* arrayToConvert, int currSize) {
	string newStr = "";
	for(int index=0; index<currSize; index++) {
		newStr += "Line Number: " + this->convertInt(index) + 
			"  Array Number: " + this->convertInt(arrayToConvert[index]) + "\n";
	}

	return newStr;

} //end function

//Returns 'unsortedArray' Member Var
int* ArrayControls::getCurrArray() { return this->currArray; } //end function

//Returns 'size' Member Var
int ArrayControls::getSize() { return size; } //end function

//==============================================================





//==============================================================
//Functions/Controls -------------------------------------------
//==============================================================

//Deletes and 'Safe's' The Given Pointer
int* ArrayControls::deleteArray(int* ptr) {
	if(ptr!=NULL) {
		delete(ptr);
		ptr = NULL;

	} 

	return ptr;

} //end function

//Returns Random int Between start & end Given
int ArrayControls::randomize(int start, int end) {
	int randomNum = rand() % end + start;

	return randomNum;

} //end function

//Fills Array With Values 0 - 'size' Member Var
void ArrayControls::fillBlankArray() {
	for(int index=0; index<size; index++) {
		this->currArray[index] = index;
	}

} //end function

//Takes int & Converts It To string Type
string ArrayControls::convertInt(int number) {
   stringstream conversion;
   conversion << number;
   
   return conversion.str();

} //end function


//Will Display Given newMsg Line With Return Char
//Console Only--
void ArrayControls::displayALine(string newMsg) {
	cout << newMsg << endl;
	
} //end function


//Will Display Given newMsg Line Without Return Char
//Console Only--
void ArrayControls::displayMsg(string newMsg) {
	cout << newMsg;
	
} //end function

//Outputs Entire Array To Screen
void ArrayControls::printArray(int* arrayToPrint, int currSize) {
	for(int index=0; index<currSize; index++) {
		cout << " " << arrayToPrint[index];
	}

} //end function

//Outputs Entire Array To Screen
void ArrayControls::printArray(string* arrayToPrint, int lineCount) {
	for(int index=0; index<lineCount; index++) {
		cout << arrayToPrint[index] << endl;
	}

} //end function

//Randomly Shuffles Array Values
void ArrayControls::unsortTheArray() {
	for(int index=0; index<size; index++) {
		this->currArray[index] = this->currArray[this->randomize(bottom, size)];
	}

} //end function

//Reverses Array into (new lowest)'size' Member Var - 0(new highest)
void ArrayControls::reverseTheArray() {
	int counter = size-1;
	for(int index=0; index<size; index++) {
		int tempNum = this->currArray[index];
		this->currArray[index] = this->currArray[counter];
		this->currArray[counter] = tempNum;
		counter--;
	}

} //end function

char** ArrayControls::MakeCharWordArray(string* currArr, int wordCount) {
	char** foundWords = new char*[wordCount];

	for(int words=0; words<wordCount; words++) {
		string blah = currArr[words];
		char* test = new char[currArr[words].length()+1];
		strcpy_s(test, currArr[words].length()+1, currArr[words].c_str());	
		
		foundWords[words] = test;
		//delete [] test;	
	}

	return foundWords;

} //end function