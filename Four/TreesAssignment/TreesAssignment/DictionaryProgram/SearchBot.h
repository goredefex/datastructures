#ifndef SEARCHBOT_H
#define SEARCHBOT_H

class SearchBot {

	//Private Member vars
	private:
		int lowerBound;
		int upperBound;
	
	//Public Methods
	public:
		//Constructs
		SearchBot(void);
		SearchBot(int newLower, int newUpper);
		~SearchBot(void);

		//Getters
		int getLowerBound();
		int getUpperBound();

		//Setters
		void setLowerBound(int newNum);
		void setUpperBound(int newNum);

		//Functions
		int sequentialSearch(int* currArray, int size, int numToFind);
		int binarySearchNonRecursive(int* currArray, int size, int numToFind);
		int binarySearch(int* currArray, int size, int numToFind);

};

#endif