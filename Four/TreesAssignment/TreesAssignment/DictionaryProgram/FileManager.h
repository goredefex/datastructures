
#ifndef H_FILEMANAGER
#define H_FILEMANAGER

#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <sstream>

using namespace std;

class FileManager {

	//Private Member Vars
	private:
		string fileName;
		char *data;
		string contents;
		string* wordArray;
		int charAmmount;
		int rowsReadIn;
		int colsReadIn;

	//Public Methods
	public:
		//Constructs
		FileManager(string newFile);
		FileManager(void);
		~FileManager(void);

		//Getters
		string getFileName();
		string getContents();
		string* getWordArray();
		int getCharAmmount();
		char* getCharData();
		int getRowsReadIn();
		int getColsReadIn();

		//Setters
		void setFileName(string newFile);
		void setContents(string newContents);
		void setWordArray(string* newArr);
		void setCharAmmount(int newAmmount);
		void setRowsReadIn(int newAmmount);
		void setColsReadIn(int newAmmount);

		//Functions
		int countFileSize();
		void countDimensions();
		void increaseRowsRead();
		void increaseColsRead();
		void addToContents(string newContents);
		void addLineToContents(string newContents);
		void wipeContents();
		void convertContentsToData();
		void tokenizeContentsStringToWordArray();
		string convertIntArray(int* intArray, int size);
		void removeLineTerminators();
		void removeSpecificChar(string charToRemove);
		bool readFile();
		bool readNoCounting();
		bool readFileIntoStringArray();
		char* deleteStringArray(char* ptr);
		void generateFile(string newFileName);
		void changeReturnChars();

};

#endif