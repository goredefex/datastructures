// DictionaryProgram.cpp 
//
// Authored By: Christian Bowyer, W0128388
// Tues, April 01, 2014
//

#include <iostream>
#include "string.h"
#include <sstream>
#include "stdafx.h"
#include "FileManager.h"
#include "TimerBox.h"
#include "ArrayControls.h"
#include "SearchBot.h"
#include "BST.h"

using namespace std;

//====================================
//Entry Point ------------------------
//====================================
int _tmain(int argc, _TCHAR* argv[]) {


	//Part 1 Objects
	TimerBox timer(true);
	ArrayControls arr;
	SearchBot search;


	//==========================================
	//Part 1 -----------------------------------
	//==========================================

	////Sequential Search
	//arr.displayALine("Searching In Sequence...");
	//arr.displayALine("Start Time: ");
	//cout << timer; cout << endl;
	//timer.startTimer();
	//for(int i=0; i<arr.getSize(); i++) { //<--Find Each Number
	//	int seqPtr = search.sequentialSearch(arr.getCurrArray(), arr.getSize(), i);	
	//	//arr.displayALine("Found " + arr.convertInt(i) + " In Position " + arr.convertInt(seqPtr));
	//}
	//timer.stopTimer();
	//arr.displayALine("Done Search!   End Time: ");
	//cout << timer; cout << " Milliseconds" << endl;

	//cin.get();

	////Binary Non-Recursive Search
	//arr.displayALine("Searching Binary Tree...");
	//arr.displayALine("Start Time: ");
	//cout << timer; cout << endl;
	//timer.startTimer();
	//for(int i=0; i<arr.getSize(); i++) { //<--Find Each Number
	//	int noRecBinPtr = search.binarySearchNonRecursive(arr.getCurrArray(), arr.getSize(), i);
	//	//arr.displayALine("Found " + arr.convertInt(i) + " In Position " + arr.convertInt(noRecBinPtr));
	//}
	//timer.stopTimer();
	//arr.displayALine("Done Search!   End Time: ");
	//cout << timer; cout << " Milliseconds" << endl;

	//cin.get();
	
	//==========================================
	//Part 2 -----------------------------------
	//==========================================

	//Part 2 Objects
	FileManager file("dictionary.txt");
	FileManager spellChecker("mispelled.txt");
	bool fileWorked = file.readFileIntoStringArray(); //<--Get Dictionary
	bool spellCheckerWorked = spellChecker.readNoCounting();
	

	//UI Spacing
	cout << endl; cout << endl;

	//Take Out Harmful Chars--------------------------
	spellChecker.removeSpecificChar("\"");
	spellChecker.removeSpecificChar("(#)");
	spellChecker.removeSpecificChar("(&)");
	spellChecker.removeSpecificChar(".");
	spellChecker.removeSpecificChar(",");
	spellChecker.removeSpecificChar("1");
	spellChecker.removeSpecificChar("2");
	spellChecker.removeSpecificChar("3");
	spellChecker.changeReturnChars();
	spellChecker.removeLineTerminators();
	spellChecker.tokenizeContentsStringToWordArray();

	//------------------------------------------------

	//Turn Dictionary Strings Into Char* Array
	char** schma = arr.MakeCharWordArray(file.getWordArray(), file.getRowsReadIn());
	
	//Send Char* Array To BST To Make Custom Tree
	BST bst;
	bst.buildAVLTree(schma, file.getRowsReadIn());
	bst.printTree();

	cin.get(); //<-- PAUSE
	
	//Turn testDoc Strings Into Char* Array
	char** spellChkList = arr.MakeCharWordArray(spellChecker.getWordArray(), spellChecker.getRowsReadIn());

	//------------------------------------------------

	cout << "Words Found Spelled Wrong Are:" << endl << endl;

	
	cout << "---------------------------------" << endl;
	timer.startTimer();
	for(int i=0; i<spellChecker.getRowsReadIn(); i++) { 
		bool test = bst.isInTree(spellChkList[i]); 
		if(!test) { cout << "Mispelled Word:  " << spellChkList[i] << endl; }
	}
	timer.stopTimer();
	cout << "---------------------------------" << endl;
	
	
	system("pause");

	return 0; //<--Exit Condition

} //end entry

//====================================

