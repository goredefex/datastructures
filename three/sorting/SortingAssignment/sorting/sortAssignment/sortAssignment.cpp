/*
	Authored By: Christian Bowyer, W0128388
	Tuesday, March, 04 2014
	Assignment 3 - Sorting [Minimal UI]

*/

#include "stdafx.h"
#include "Business.h"
#include "Sorter.h"
#include "TimerBox.h"
#include "FileManager.h"
#include <string>

//Entry Point ===============================
int _tmain(int argc, _TCHAR* argv[]) {

	Business biz;
	Sorter sort;
	TimerBox timer;
	TimerBox miliTimer(true); //<--Set To Miliseconds

	biz.unsortTheArray(); //<-- Jumble Array

	// Constant Size Sorting  ===================================================

	////Bubble Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//miliTimer.startTimer();
	//int* tempPtrB = sort.BubbleSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//miliTimer.stopTimer();
	//biz.displayMsg("Bubble Sort Completed! Time: ");
	//cout << miliTimer; cout << endl;

	//biz.printArray(tempPtrB, biz.getSize());

	////Selection Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//miliTimer.startTimer();
	//int* tempPtrS = sort.SelectionSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//miliTimer.stopTimer();
	//biz.displayMsg("Selection Sort Completed! Time: ");
	//cout << miliTimer; cout << endl;

	////Insertion Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//miliTimer.startTimer();
	//int* tempPtrI = sort.InsertionSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//miliTimer.stopTimer();
	//biz.displayMsg("Insertion Sort Completed! Time: ");
	//cout << miliTimer; cout << endl;
	
	////Merge Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//miliTimer.startTimer();
	//int* tempPtrM = sort.MergeSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//miliTimer.stopTimer();
	//biz.displayMsg("Sort Merge Completed! Time: ");
	//cout << miliTimer << " Miliseconds"; cout << endl;

	//Quick Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//miliTimer.startTimer();
	//int* tempPtrQ = sort.MergeSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//miliTimer.stopTimer();
	//biz.displayMsg("Sort Quick Completed! Time: ");
	//cout << miliTimer << " Miliseconds"; cout << endl;



	// =====================================================================



	// In place sorting  =============== -Static Only- =====================

	//External Merge Sort - Step 1 Unsorted
	FileManager outputFile;
	outputFile.setContents(biz.getArrayString(biz.getUnsortedArray(), biz.getSize()));
	outputFile.generateFile("tester/UnsortedList.txt");

	//External Merge Sort - Step 2 Making The Two Sublist Files
	FileManager l;
	FileManager r;
	FileManager* leftFileToPrint = &l;
	FileManager* rightFileToPrint = &r;
	int* beginningArray = biz.getUnsortedArray();
	int* newArray[1000];
	int *allBlocks[100];
	int leftBound = 0;
	int rightBound = 999;

	//Block Solving/Size = 1000
	for(int k=0; k<100; k++) { //<--Fill arrays
		for(int i=leftBound; i<rightBound; i++) {
			newArray[i-(1000*k)] = &beginningArray[i];
		}
		leftBound += 1000;
		rightBound += 1000;
		allBlocks[k] = *newArray;
	}
	
	//Solve Arrays
	for(int counter=0; counter<100; counter++) {
		l.wipeContents();
		r.wipeContents();
		allBlocks[counter] = sort.ExternalMergeSort(allBlocks[counter], 1000, leftFileToPrint, rightFileToPrint);
		l.generateFile("tester/leftSubList" + biz.convertInt(counter) + ".txt");
		r.generateFile("tester/rightSubList" + biz.convertInt(counter) + ".txt");

	}
	
	int* finalExternal = NULL;
	l.wipeContents();
	r.wipeContents();

	//Compiling all arrays to final merge
	for(int index=0; index<100; index++) {
		if(index==0) {
			finalExternal = sort.ExternalMerge(allBlocks[index], allBlocks[index+1], 1000, 1000, leftFileToPrint, rightFileToPrint);	
		} else if(index!=1) {
			finalExternal = sort.ExternalMerge(finalExternal, allBlocks[index], (index*1000), 1000, leftFileToPrint, rightFileToPrint);
		}
	}

	//External Merge Sort - Step 3 Re-Merge Files
	outputFile.setContents(outputFile.convertIntArray(finalExternal, biz.getSize()));
	outputFile.generateFile("tester/FinalMerge.txt");
	
	// =====================================================================


	//biz.printArray(tempPtrQ, biz.getSize()); //<-- Test Unit

	cout << endl;
	system("pause");
	
	return 0;

} //end entry point

// =========================================

