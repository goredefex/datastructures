#include "stdafx.h"
#include "Business.h"


// =======================================================================
// Constructs :
// =======================================================================

Business::Business(void):size(100000), bottom(0) {

	srand(time(0));
	this->unsortedArray = new int[size];
	this->fillBlankArray();
		
} //end constructor


Business::~Business(void) {
	this->unsortedArray = this->deleteArray(this->unsortedArray);
} //end destructor


// ==========================================




// =======================================================================
// Private :
// =======================================================================


//Functions ---------------------------------
int* Business::deleteArray(int* ptr) {
	if(ptr!=NULL) {
		delete(ptr);
		ptr = NULL;

	} 

	return ptr;

} //end function

int Business::randomize(int start, int end) {
	int randomNum = rand() % end + start;

	return randomNum;

} //end function

void Business::fillBlankArray() {
	for(int index=0; index<size; index++) {
		this->unsortedArray[index] = index;
	}

} //end function

string Business::convertInt(int number) {
   stringstream conversion;
   conversion << number;
   
   return conversion.str();

} //end function



// =======================================================================
// Public :
// =======================================================================


//Functions ---------------------------------
void Business::displayALine(string newMsg) {
	cout << newMsg << endl;
	
} //end function

void Business::displayMsg(string newMsg) {
	cout << newMsg;
	
} //end function

void Business::printArray(int* arrayToPrint, int currSize) {
	for(int index=0; index<currSize; index++) {
		cout << " " << arrayToPrint[index];
	}

} //end function

string Business::getArrayString(int* arrayToConvert, int currSize) {
	string newStr = "";
	for(int index=0; index<currSize; index++) {
		newStr += "Line Number: " + this->convertInt(index) + 
			"  Array Number: " + this->convertInt(arrayToConvert[index]) + "\n";
	}

	return newStr;

} //end function

int* Business::getUnsortedArray() {
	return this->unsortedArray;

} //end function

int Business::getSize() {
	return size;

} //end function

void Business::unsortTheArray() {
	for(int index=0; index<size; index++) {
		this->unsortedArray[index] = this->unsortedArray[this->randomize(bottom, size)];
	}

} //end function

void Business::reverseTheArray() {
	int counter = size-1;
	for(int index=0; index<size; index++) {
		int tempNum = this->unsortedArray[index];
		this->unsortedArray[index] = this->unsortedArray[counter];
		this->unsortedArray[counter] = tempNum;
		counter--;
	}

} //end function
