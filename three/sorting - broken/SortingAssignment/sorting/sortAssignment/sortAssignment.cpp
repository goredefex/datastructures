/*
	Authored By: Christian Bowyer, W0128388
	Tuesday, March, 04 2014
	Assignment 3 - Sorting [Minimal UI]

*/

#include "stdafx.h"
#include "Business.h"
#include "Sorter.h"
#include "TimerBox.h"
#include "FileManager.h"
#include <string>

//Entry Point ===============================
int _tmain(int argc, _TCHAR* argv[]) {

	Business biz;
	Sorter sort;
	TimerBox timer;
	TimerBox miliTimer(true); //<--Set To Miliseconds

	biz.unsortTheArray(); //<-- Jumble Array

	// Constant Size Sorting  ===================================================

	////Bubble Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//timer.startTimer();
	//int* tempPtrB = sort.BubbleSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//timer.stopTimer();
	//biz.displayMsg("Bubble Sort Completed! Time: ");
	//cout << timer; cout << endl;

	////Selection Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//timer.startTimer();
	//int* tempPtrS = sort.SelectionSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//timer.stopTimer();
	//biz.displayMsg("Selection Sort Completed! Time: ");
	//cout << timer; cout << endl;

	////Insertion Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//timer.startTimer();
	//int* tempPtrI = sort.InsertionSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//timer.stopTimer();
	//biz.displayMsg("Insertion Sort Completed! Time: ");
	//cout << timer; cout << endl;
	//
	////Merge Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//miliTimer.startTimer();
	//int* tempPtrM = sort.MergeSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//miliTimer.stopTimer();
	//biz.displayMsg("Sort Merge Completed! Time: ");
	//cout << miliTimer << " Miliseconds"; cout << endl;

	////Quick Sorting
	//biz.displayALine("Sorting! Start Time: 0");
	//miliTimer.startTimer();
	//int* tempPtrQ = sort.MergeSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	//miliTimer.stopTimer();
	//biz.displayMsg("Sort Quick Completed! Time: ");
	//cout << miliTimer << " Miliseconds"; cout << endl;



	// =====================================================================



	// In place sorting  ===================================================

	//External Merge Sort - Step 1 Unsorted
	FileManager outputFile;
	outputFile.setContents(biz.getArrayString(biz.getUnsortedArray(), biz.getSize()));
	outputFile.generateFile("tester/UnsortedList.txt");

	//External Merge Sort - Step 2 Making The Two Sublist Files
	FileManager l;
	FileManager r;
	FileManager* leftFileToPrint = &l;
	FileManager* rightFileToPrint = &r;
	int* finalExternal = sort.ExternalMergeSort(biz.getUnsortedArray(), biz.getSize(), leftFileToPrint, rightFileToPrint);
	l.generateFile("tester/LeftSubList.txt");
	r.generateFile("tester/RightSubList.txt");
		
	//External Merge Sort - Step 3 Re-Merge Files
	outputFile.setContents(outputFile.convertIntArray(finalExternal, biz.getSize()));
	outputFile.generateFile("tester/FinalMerge.txt");
	
	// =====================================================================


	//biz.printArray(tempPtrQ, biz.getSize());

	cout << endl;
	system("pause");
	
	return 0;

} //end entry point

// =========================================

