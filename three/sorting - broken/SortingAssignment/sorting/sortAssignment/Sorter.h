#ifndef SORTER_H
#define SORTER_H

#include "FileManager.h"

class Sorter {

	private:
		//Methods
		int* Merge(int* leftArray, int* rightArray, int leftSize, int rightSize);

	public:
		//Constructs
		Sorter(void);
		~Sorter(void);

		//Methods
		int* BubbleSort(int* unsortedArray, int size);
		int* SelectionSort(int* unsortedArray, int size);
		int* InsertionSort(int* unsortedArray, int size);
		int* MergeSort(int* unsortedArray, int size);
		int* QuickSort(int* unsortedArray, int size);
		void QuickSortRecursion(int* unsortedArray, int startPos, int endPos);
		int Partition(int* unsortedArray, int startPos, int endPos);
		int* ExternalMergeSort(int* unsortedArray, int size, FileManager* leftFile, FileManager* rightFile);
		int* ExternalMerge(int* leftArray, int* rightArray, int leftSize, int rightSize, FileManager* leftFile, FileManager* rightFile);
		
};

#endif