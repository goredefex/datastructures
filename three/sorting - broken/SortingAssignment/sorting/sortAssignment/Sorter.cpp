#include "stdafx.h"
#include "Sorter.h"


// =======================================================================
// Contructs :
// =======================================================================

Sorter::Sorter(void) {


} //end constructor


Sorter::~Sorter(void) {


} //end destructor

// ==========================================



// =======================================================================
// Public :
// =======================================================================


//Functions --------------------------------
int* Sorter::BubbleSort(int* unsortedArray, int size) {

	int end = size-1;

	for(int index=0; index<end; index++) {
		//Find Highest Bubble
		if(unsortedArray[index+1]<unsortedArray[index]) {
			int tempInt = unsortedArray[index];
			unsortedArray[index] = unsortedArray[index+1];
			unsortedArray[index+1] = tempInt;
		}

		//Done Bubbling A Number
		if((index+1) == end && end > 0) {
			end--;
			index = 0;

		} 
	}

	return unsortedArray;

} //end function


int* Sorter::SelectionSort(int* unsortedArray, int size) {

	int lowest = 0;
	int current = 0;

	while(current<size) {
		lowest = current;
		for(int index=current; index<size; index++) {
			if(unsortedArray[index]<unsortedArray[lowest]) {
				lowest = index;
			}

			if((index+1) == size) {
				int tempNum = unsortedArray[current];
				int test = unsortedArray[lowest];
				unsortedArray[current] = unsortedArray[lowest];
				unsortedArray[lowest] = tempNum;
				current++;
			}
		}

	}

	return unsortedArray;

} //end function


int* Sorter::InsertionSort(int* unsortedArray, int size) {

	int* sortedArray = new int[size];
	int currentSize = 0;
	sortedArray[currentSize] = unsortedArray[currentSize]; //<--Insert Beginning
	currentSize++;
	bool sliding = false;

	for(int index=1; index<size; index++) {
		sliding = false;
		int blah = unsortedArray[index];
		for(int sortedIndex=currentSize; sortedIndex>0; sortedIndex--) {
			//Engage Sliding
			if(unsortedArray[index] < sortedArray[sortedIndex-1]) {
				sortedArray[sortedIndex] = sortedArray[sortedIndex-1];
				sortedArray[sortedIndex-1] = -1; //<--Make Invalid
				sliding = true;
			} 
			
			//Place Indicies
			if(unsortedArray[index] >= sortedArray[sortedIndex-2] && sliding) {
				
				sortedArray[sortedIndex-1] = unsortedArray[index]; 
				currentSize++;
				sortedIndex = 0;

			} else if(unsortedArray[index] >= sortedArray[sortedIndex-1] && !sliding) {

				sortedArray[sortedIndex] = unsortedArray[index]; 
				currentSize++;
				sortedIndex = 0;
			}

			if(sortedIndex==1 && unsortedArray[index] == 0) {
				currentSize++;
			}
			
		}
	}

	return sortedArray;

} //end function


int* Sorter::MergeSort(int* unsortedArray, int size) {

	int leftSize = 0;
	int rightSize = 0;
	int* left = NULL;
	int* right = NULL;

	if(size>2) {
		//Find Array Size
		leftSize = size / 2;
		rightSize = size - leftSize;
		//Create Arrays
		left = new int[leftSize];
		right = new int[rightSize];
		//Fill New Partitions
		for(int index=0; index<leftSize; index++) {
			left[index] = unsortedArray[index];
		}
		for(int index=leftSize; index<size; index++) {
			right[index-leftSize] = unsortedArray[index];		
		}

		//Recursive Implementation
		left = this->MergeSort(left, leftSize);
		right = this->MergeSort(right, rightSize);

		unsortedArray = this->Merge(left, right, leftSize, rightSize);
			
	} else if(size == 2) {
		if(unsortedArray[0] > unsortedArray[1]) {
			int tempNum = unsortedArray[0];
			unsortedArray[0] = unsortedArray[1];
			unsortedArray[1] = tempNum;	
		}
	}
	
	return unsortedArray;

} //end function


int* Sorter::Merge(int* leftArray, int* rightArray, int leftSize, int rightSize) {
	
	int* mergedArray = new int[leftSize+rightSize];

	if(leftSize!=0 || rightSize!=0) { //<--Catch 
		int counter = 0;
		int leftPointer = 0;
		int rightPointer = 0;
		
		//Usable Numbers In Both Arrays
		while(leftPointer<leftSize && rightPointer<rightSize) {
			if(leftArray[leftPointer]<=rightArray[rightPointer]) {
				mergedArray[counter] = leftArray[leftPointer];
				leftPointer++;

			} else {
				mergedArray[counter] = rightArray[rightPointer];
				rightPointer++;
			
			}
			counter++;
		}

		//Usable Numbers In Left Array
		while(leftPointer<leftSize) {
			mergedArray[counter] = leftArray[leftPointer];
			leftPointer++;
			counter++;
		}

		//Usable Numbers In Right Array
		while(rightPointer<rightSize) {
			mergedArray[counter] = rightArray[rightPointer];
			rightPointer++;
			counter++;
		}


	} else {
		mergedArray = NULL;

	}

	return mergedArray;

} //end function


int* Sorter::QuickSort(int* unsortedArray, int size) {
	this->QuickSortRecursion(unsortedArray, 0, (size-1));
	return unsortedArray;

} //end function


void Sorter::QuickSortRecursion(int* unsortedArray, int startPos, int endPos) {

	if(startPos < endPos) {
		int pointer = this->Partition(unsortedArray, startPos, endPos);
		this->QuickSortRecursion(unsortedArray, startPos, (pointer-1));
		this->QuickSortRecursion(unsortedArray, (pointer+1), endPos);
	}

} //end function


int Sorter::Partition(int* unsortedArray, int startPos, int endPos) {
	
	int pivot = unsortedArray[endPos];
	int pointer = startPos;
	
	//Begin Arranging Pivot
	for(int index=startPos; index<endPos; index++) {
		if(unsortedArray[index] <= pivot) {
			int tempNum = unsortedArray[index];
			unsortedArray[index] = unsortedArray[pointer];
			unsortedArray[pointer] = tempNum;
			pointer++;
		}
	}

	int tempNum = unsortedArray[pointer];
	unsortedArray[pointer] = unsortedArray[endPos];
	unsortedArray[endPos] = tempNum;

	return pointer;

} //end funtion


int* Sorter::ExternalMergeSort(int* unsortedArray, int size, FileManager* leftFile, FileManager* rightFile) {

	int leftSize = 0;
	int rightSize = 0;
	int* left = NULL;
	int* right = NULL;

	if(size>2) {
		//Find Array Size
		leftSize = size / 2;
		rightSize = size - leftSize;
		//Create Arrays
		left = new int[leftSize];
		right = new int[rightSize];
		//Fill New Partitions
		for(int index=0; index<leftSize; index++) {
			left[index] = unsortedArray[index];
		}
		for(int index=leftSize; index<size; index++) {
			right[index-leftSize] = unsortedArray[index];		
		}

		//Recursive Implementation
		left = this->ExternalMergeSort(left, leftSize, leftFile, rightFile);
		right = this->ExternalMergeSort(right, rightSize, leftFile, rightFile);

		unsortedArray = this->ExternalMerge(left, right, leftSize, rightSize, leftFile, rightFile);
					
	} else if(size == 2) {
		if(unsortedArray[0] > unsortedArray[1]) {
			int tempNum = unsortedArray[0];
			unsortedArray[0] = unsortedArray[1];
			unsortedArray[1] = tempNum;	
		}
	}
	
	return unsortedArray;

} //end function


int* Sorter::ExternalMerge(int* leftArray, int* rightArray, int leftSize, int rightSize, FileManager* leftFile, FileManager* rightFile) {
	
	//External Update
	leftFile->addToContents("Left Array: \n" + leftFile->convertIntArray(leftArray, leftSize) + "\n");
	rightFile->addToContents("Right Array: \n" + rightFile->convertIntArray(rightArray, rightSize) + "\n");

	int* mergedArray = new int[leftSize+rightSize];

	if(leftSize!=0 || rightSize!=0) { //<--Catch 
		int counter = 0;
		int leftPointer = 0;
		int rightPointer = 0;
		
		//Usable Numbers In Both Arrays
		while(leftPointer<leftSize && rightPointer<rightSize) {
			if(leftArray[leftPointer]<=rightArray[rightPointer]) {
				mergedArray[counter] = leftArray[leftPointer];
				leftPointer++;

			} else {
				mergedArray[counter] = rightArray[rightPointer];
				rightPointer++;
			
			}
			counter++;
		}

		//Usable Numbers In Left Array
		while(leftPointer<leftSize) {
			mergedArray[counter] = leftArray[leftPointer];
			leftPointer++;
			counter++;
		}

		//Usable Numbers In Right Array
		while(rightPointer<rightSize) {
			mergedArray[counter] = rightArray[rightPointer];
			rightPointer++;
			counter++;
		}


	} else {
		mergedArray = NULL;

	}

	return mergedArray;

} //end function