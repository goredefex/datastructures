
#ifndef H_FILEMANAGER
#define H_FILEMANAGER

#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <sstream>

using namespace std;

class FileManager {

	//Private Member Vars
	private:
		string fileName;
		char *data;
		string contents;
		int charAmmount;
		int rowsReadIn;
		int colsReadIn;

	//Public Methods
	public:
		//Constructs
		FileManager(string newFile);
		FileManager(void);
		~FileManager(void);

		//Getters
		string getFileName();
		string getContents();
		int getCharAmmount();
		char* getCharData();
		int getRowsReadIn();
		int getColsReadIn();

		//Setters
		void setFileName(string newFile);
		void setContents(string newContents);
		void setCharAmmount(int newAmmount);
		void setRowsReadIn(int newAmmount);
		void setColsReadIn(int newAmmount);

		//Functions
		int countFileSize();
		void countDimensions();
		void increaseRowsRead();
		void increaseColsRead();
		void addToContents(string newContents);
		void convertContentsToData();
		string convertIntArray(int* intArray, int size);
		void removeLineTerminators();
		bool readFile(int size);
		char* deleteStringArray(char* ptr);
		void generateFile(string newFileName);
		void changeReturnChars();

};

#endif