#ifndef BUSINESS_H
#define BUSINESS_H

#include <string>
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <sstream>

using namespace std;

class Business {

	const int size;
	const int bottom;

	private:
		//Member Vars
		int* unsortedArray;

		//Methods
		int* deleteArray(int* ptr);
		int randomize(int start, int end);
		void fillBlankArray();
		string convertInt(int newNum);
				
	public:
		//Constructs
		Business(void);
		~Business(void);

		//Methods
		void printArray(int* arrayToPrint, int currSize);
		string getArrayString(int* arrayToConvert, int currSize);
		int* getUnsortedArray();
		int getSize();
		void displayALine(string newMsg);
		void displayMsg(string newMsg);
		void unsortTheArray();
		void reverseTheArray();

};

#endif