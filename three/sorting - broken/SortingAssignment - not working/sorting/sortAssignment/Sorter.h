#ifndef SORTER_H
#define SORTER_H

class Sorter {

	private:
		//Methods
		int* Merge(int* leftArray, int* rightArray, int leftSize, int rightSize);

	public:
		//Constructs
		Sorter(void);
		~Sorter(void);

		//Methods
		int* BubbleSort(int* unsortedArray, int size);
		int* SelectionSort(int* unsortedArray, int size);
		int* InsertionSort(int* unsortedArray, int size);
		int* MergeSort(int* unsortedArray, int size);
		int* QuickSort(int* unsortedArray, int size);
		
};

#endif