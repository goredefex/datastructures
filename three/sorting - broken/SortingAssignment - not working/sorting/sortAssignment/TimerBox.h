#ifndef TIMERBOX_H
#define TIMERBOX_H

#include <time.h>
#include <iostream>
#include <string>
using namespace std;

class TimerBox {
	
	friend ostream& operator<<(ostream& stream, TimerBox& timer); //<--No Line Term

	//Private Member Vars
	private:
		clock_t start, end;
		double timePassed;
		int timeVariant;

	//Public Methods
	public:
		//Constructs
		TimerBox(void);
		TimerBox(bool miliseconds);

		//Getters
		clock_t getStart();
		clock_t getEnd();
		double getTimePassed();

		//Setters
		void setStart(clock_t newTime);
		void setEnd(clock_t newTime);
		void setTimePassed(double newTime);

		//Functions
		clock_t variate(clock_t currTime);
		void checkTimePassed();
		void stopTimer();
		void startTimer();

};

#endif