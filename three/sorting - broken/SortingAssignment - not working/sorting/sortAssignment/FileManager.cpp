
#include "stdafx.h"
#include "FileManager.h"

//Constructor --------------------------------------------------
FileManager::FileManager(string newFile) {
	this->setFileName(newFile);
	this->setRowsReadIn(0);
	this->setColsReadIn(0);
	
} //end constructor

//Default For Pre-Instances
FileManager::FileManager(void) {} //end constructor

//Destructor ---------------------------------------------------
FileManager::~FileManager(void) {
	this->data = deleteStringArray(this->data);

} //end destructor




//Getters ------------------------------------------------------
string FileManager::getFileName() {
	return this->fileName;

} //end function

string FileManager::getContents() {
	return this->contents;

} //end function

int FileManager::getCharAmmount() {
	return this->charAmmount;

} //end function

char* FileManager::getCharData() {
	return this->data;

} //end function

int FileManager::getRowsReadIn() {
	return this->rowsReadIn;

} //end function

int FileManager::getColsReadIn() {
	return this->colsReadIn;

} //end function





//Setters ------------------------------------------------------
void FileManager::setFileName(string newFile) {
	this->fileName = "c:/"+newFile;

} //end function

void FileManager::setContents(string newContents) {
	this->contents = newContents;

} //end function

void FileManager::setCharAmmount(int newAmmount) {
	this->charAmmount = newAmmount;

} //end function

void FileManager::setRowsReadIn(int newAmmount) {
	this->rowsReadIn = newAmmount;

} //end function

void FileManager::setColsReadIn(int newAmmount) {
	this->colsReadIn = newAmmount;

} //end function




//Functions ------------------------------------------------------
char* FileManager::deleteStringArray(char* ptr) {
	
	if(ptr!=NULL) {
		delete(ptr);
		ptr = NULL;

	} 

	return ptr;

} //end function

void FileManager::addToContents(string newContents) {
	this->contents += newContents;

} //end function

void FileManager::convertContentsToData() {
	this->data = &this->contents[0];

} //end function

void FileManager::increaseRowsRead() {
	this->rowsReadIn++;

} //end function

void FileManager::increaseColsRead() {
	this->colsReadIn++;

} //end function


void FileManager::changeReturnChars() {

	size_t currCharToReplace = string::npos;

	do {
		currCharToReplace = this->contents.find_first_of("\r");
		if(currCharToReplace != string::npos) {
			this->contents[currCharToReplace] = *"\n";
		}
	
	} while(currCharToReplace!=string::npos);

} //end function

void FileManager::removeLineTerminators() {
	
	size_t currCharToRemove = string::npos;

	do {
		string charToFind = "\n";
		currCharToRemove = this->contents.find_first_of(charToFind);
		
		if(currCharToRemove != string::npos) {
			this->contents[currCharToRemove] = *"";
		}
	
	} while(currCharToRemove!=string::npos);

} //end function

void FileManager::countDimensions() {
	
	size_t searchChar = string::npos;
	size_t trailChar = 0;
	bool foundColLength = false;

	do {
		searchChar = this->contents.find_first_of("\n", trailChar);
		this->increaseRowsRead();
		if(!foundColLength) {
			this->setColsReadIn(searchChar);
			foundColLength = true;
		}
		trailChar = searchChar + 1;

	} while(searchChar!=string::npos);

} //end function

int FileManager::countFileSize() {
	
	//Total Initiated
	int counter = 0;

	ifstream inFile; 
	inFile.exceptions(ios::badbit);

	try {

		inFile.open(this->getFileName(), std::ifstream::binary); 
		
		if(inFile.fail()) {
			cout << "Cannot Read Filename. Please Re-enter..." << endl;
			throw "Cannot Read File For Counting";

		} else {

			while (inFile.good()) {
				inFile.get(); 
				counter++;

			}

			//Protect Against Breaks
			try {
				inFile.close();
			} catch(std::ios_base::failure& e) {
				cout << "Could Not Close Size Counter" << endl;
			}

		}
		
	} catch(string err) {
		
		cout << err << endl;
	}
	
	//Update Char Totals
	this->setCharAmmount(counter);

	return counter;

} //end function


bool FileManager::readFile(int size) {
	
	ifstream inFile; 
	inFile.exceptions(std::ios::badbit | std::ios::failbit);
	bool valid = false;

	try {

		inFile.open(this->getFileName(), std::ifstream::binary); 

		if(!inFile.fail()) {
			
			while (inFile.good()) {
				string token;
				getline(inFile, token); 
				this->addToContents(token);

			}

			//Protect Against Breaks
			try {
				inFile.close();
				valid = true;
			} catch(std::ios_base::failure& e) {
				cout << "Could Not Close Reader" << endl;
			}

		}

	} catch(std::exception const& err) {
		
		cout << err.what() << "Could Not Open File To Read!" << endl;
		valid = false;
	}

	
	//Char Chars For Reading
	this->changeReturnChars();

	//Update Row/Col Totals
	this->countDimensions();
	
	return valid;

} //end function


void FileManager::generateFile(string newFileName) {

	try {

		ofstream newFile("c:/"+newFileName);
		newFile.exceptions(std::ios::badbit);

		if(!newFile.fail()) {
			newFile << this->getContents();

		}

	} catch(...) {
		cout << "Cannot Generate File" << endl;

	}

} //end function
