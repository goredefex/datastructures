/*
	Authored By: Christian Bowyer, W0128388
	Tuesday, March, 04 2014
	Assignment 3 - Sorting [Minimal UI]

*/

#include "stdafx.h"
#include "Business.h"
#include "Sorter.h"
#include "TimerBox.h"
#include "FileManager.h"
#include <string>

//Entry Point ===============================
int _tmain(int argc, _TCHAR* argv[]) {

	Business biz;
	Sorter sort;
	TimerBox timer;
	TimerBox miliTimer(true); //<--Set To Miliseconds

	biz.unsortTheArray();

	//Selection Sorting
	biz.displayALine("Sorting! Start Time: 0");
	int* tempPtrB = sort.BubbleSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	timer.stopTimer();
	biz.displayMsg("Sort Completed! Time: ");
	cout << timer; cout << endl;

	//Selection Sorting
	biz.displayALine("Sorting! Start Time: 0");
	int* tempPtrS = sort.SelectionSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	timer.stopTimer();
	biz.displayMsg("Sort Completed! Time: ");
	cout << timer; cout << endl;

	//Insertion Sorting
	biz.displayALine("Sorting! Start Time: 0");
	int* tempPtrI = sort.InsertionSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	timer.stopTimer();
	biz.displayMsg("Sort Completed! Time: ");
	cout << timer; cout << endl;
	
	//Merge Sorting
	biz.displayALine("Sorting! Start Time: 0");
	miliTimer.stopTimer();
	int* tempPtrM = sort.MergeSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	miliTimer.stopTimer();
	biz.displayMsg("Sort Completed! Time: ");
	cout << miliTimer; cout << endl;

	/*int test[11] = {4, 2, 7, 6, 4, 8, 1, 9, 0, 3, 10};
	int* testPtr = test;*/

	//Quick Sorting
	biz.displayALine("Sorting! Start Time: 0");
	miliTimer.stopTimer();
	int* tempPtrQ = sort.MergeSort(biz.getUnsortedArray(), biz.getSize()); //<--Sort
	miliTimer.stopTimer();
	biz.displayMsg("Sort Completed! Time: ");
	cout << miliTimer; cout << endl;


	//External Merge Sort - Step 1 Unsorted
	FileManager outputFile;
	outputFile.setContents(biz.getArrayString(biz.getUnsortedArray(), biz.getSize()));
	outputFile.generateFile("tester/Testermain.txt");

	//External Merge Sort - Step 2 Sorting
	FileManager inputFile("tester/Testermain.txt");
	int* foundArray = inputFile.findInputNumbers();
	int newSizing = inputFile.getRowsReadIn();
	foundArray = sort.MergeSort(foundArray, newSizing);
	
	//External Merge Sort - Step 3 Making The Two Merge Files
	FileManager file1;
	file1.setContents(biz.getArrayString(foundArray, newSizing));
	file1.generateFile("leftArray.txt");
	FileManager file2;
	file2.setContents(biz.getArrayString(foundArray, newSizing));
	file2.generateFile("rightArray.txt");

	//External Merge Sort - Step 4 Re-Merge Files
	file1.setContents(file1.getContents() + file2.getContents());
	file1.generateFile(biz.getArrayString(sort.MergeSort(file1.findInputNumbers(), file1.getContents().length), file1.getContents().length));


	//biz.printArray(tempPtrM, biz.getSize());

	cout << endl;
	system("pause");



	return 0;

} //end entry point

// =========================================

