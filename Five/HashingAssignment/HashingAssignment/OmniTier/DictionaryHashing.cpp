// OmniTier.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "FileManager.h"
#include "TimerBox.h"
#include "HashTable.h"
#include <iostream>




//==============================================================
//Prototyping --------------------------------------------------
//==============================================================

//Char Array Prototype
char** MakeCharWordArray(string* currArr, int wordCount);




//==============================================================
//Entry Point --------------------------------------------------
//==============================================================

int _tmain(int argc, _TCHAR* argv[]) {
	

	//Entered Files ----------------------------------
	FileManager file("dictionary.txt");
	FileManager spellChecker("mispelled.txt");
	bool fileWorked = file.readFileIntoStringArray(); //<--Get Dictionary
	bool spellCheckerWorked = spellChecker.readNoCounting();

	//------------------------------------------------
		

	//Take Out Harmful Chars--------------------------
	spellChecker.removeSpecificChar("\"");
	spellChecker.removeSpecificChar("(#)");
	spellChecker.removeSpecificChar("(&)");
	spellChecker.removeSpecificChar(".");
	spellChecker.removeSpecificChar(",");
	spellChecker.removeSpecificChar("1");
	spellChecker.removeSpecificChar("2");
	spellChecker.removeSpecificChar("3");
	spellChecker.changeReturnChars();
	spellChecker.removeLineTerminators();
	spellChecker.tokenizeContentsStringToWordArray();

	//------------------------------------------------
	

	//Turn Dictionary Strings Into Char* Array--------
	char** dictionary = MakeCharWordArray(file.getWordArray(), file.getRowsReadIn());

	//------------------------------------------------
	
	//Turn Sample Txt Strings Into Char* Array--------
	char** spellChkList = MakeCharWordArray(spellChecker.getWordArray(), spellChecker.getRowsReadIn());
	//------------------------------------------------


	//Build Tree & Table -----------------------------

	//Send Char* Array To BST To Make Custom Tree
	BST bst;
	bst.buildAVLTree(dictionary, file.getRowsReadIn());

	//Send Char* Array To Make Custom Table
	HashTable hTable;
	for(int i=0; i<file.getRowsReadIn(); i++) { 
		string cc = dictionary[i];
		if(cc.compare("")!=0) { 
			hTable.put(dictionary[i]); 
		} 
	}
	cout << hTable;

	//------------------------------------------------
	
	cout << "Push Enter To Continue........" << endl;
	cin.get(); //<--PAUSE

	//Timing Of Functions ----------------------------

	//Begin Timing 
	TimerBox timer(true);

	//UI Spacing
	cout << endl << endl;

	//BST Timing
	cout << "Starting Binary-Search-Tree preOrder Search";
	cout << "Start Time: " << timer << endl << endl;
	cout << "---------------------------------" << endl;
	timer.startTimer();
	for(int i=0; i<spellChecker.getRowsReadIn(); i++) { 
		bool test = bst.isInTree(spellChkList[i]); 
		if(!test) { cout << "Mispelled Word:  " << spellChkList[i] << endl; }
	}
	timer.stopTimer();
	cout << "---------------------------------" << endl;
	cout << endl << "Done Searching!  End Time:  " << timer << " Milliseconds";


	//UI Spacing
	cout << endl << endl << endl << endl;


	//Hash Timing
	cout << "Starting Hash Table Search";
	cout << "Start Time: "; cout << timer; cout << endl << endl;
	cout << "---------------------------------" << endl;
	timer.startTimer();
	for(int i=0; i<spellChecker.getRowsReadIn(); i++) { 
		bool test = hTable.searchTable(spellChkList[i]); 
		if(!test) { cout << "Mispelled Word:  " << spellChkList[i] << endl; }
	}
	timer.stopTimer();
	cout << "---------------------------------" << endl;
	cout << endl << "Done Searching!  End Time:  " << timer << " Milliseconds" << endl;

	//------------------------------------------------
	
	cin.get();

	return 0;

} //end entry point

//------------------------------------------------


//Turns String Set Into Char Array
char** MakeCharWordArray(string* currArr, int wordCount) {
	char** foundWords = new char*[wordCount];

	for(int words=0; words<wordCount; words++) {
		string blah = currArr[words];
		char* test = new char[currArr[words].length()+1];
		strcpy_s(test, currArr[words].length()+1, currArr[words].c_str());	
		
		foundWords[words] = test;
		
	}

	return foundWords;

} //end function

//==============================================================