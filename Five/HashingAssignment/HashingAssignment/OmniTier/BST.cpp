#include "stdafx.h"
#include "string"
#include "BST.h"
#include <iostream>

//==============================================================
//Constructs ---------------------------------------------------
//==============================================================

//Constructor
BST::BST(void) {
	this->treeRoot = NULL;

} //end constructor


//Destructor
BST::~BST(void) { 
	if(this->treeRoot!=NULL) 
		delete(this->treeRoot);
	this->treeRoot==NULL;
} //end destructor

//==============================================================




//==============================================================
//Functions ----------------------------------------------------
//==============================================================

//Builds An AVL Tree Based On Char Arrays
void BST::buildAVLTree(char** newData, int wordCount) {
	//Build Loop
	for(int words=0; words<wordCount; words++) { this->InsertLeaf(newData[words]); }

} //end function


//Makes an Empty Tree Leaf Node
BST::Leaf* BST::MakeLeaf(char* newData, Leaf* currParent) {
	Leaf* temp = new Leaf;
	temp->data = newData;
	temp->left = NULL;
	temp->right = NULL;
	temp->parent = currParent;
	temp->leafHeight = 0;
	temp->rightHeight = 0;
	temp->leftHeight = 0;
	
	return temp;

} //end function


//Private: Recursive
//Prints A Tree To The Console Window
void BST::printTree(Leaf* p, int &currNum) {
	if (p!=NULL) {
		currNum++;
		printTree(p->right, currNum);
		
		char* charDisp = "";
		for(int i=0; i<(currNum*3); i++) { 
			cout << *charDisp; 
		
		} 
		cout << "/" << p->data << endl;
		cout << endl;
		
		printTree(p->left, currNum);
		currNum--;
	}
}


//Public:
//Prints A Tree To The Console Window
void BST::printTree() {
	int counter = 0;
	this->printTree(this->treeRoot, counter);

} //end function

//Searches For Lowest Value In Tree
void BST::findMin(Leaf* root) {

	Leaf* currLeaf = root;
	
	//Find Leftmost Leaf
	bool searching = true; //<--Loop Control
	while(searching) {
		if (currLeaf->left!=NULL) {
			searching=false;
		} else {
			currLeaf = currLeaf->left;
		}
	}    

} //end function


//Searches The Tree For Given Char Array
bool BST::isInTree(char* newData) {
	
	bool found = false;
	if(this->treeRoot!=NULL) {
		Leaf* currRoot = this->treeRoot;
		
		if(newData) {
			bool searching = true;
			while(searching) {
		
				if(strcmp(newData, currRoot->data)==0) {
					found = true;
					searching = false;

				} else if(strcmp(newData, currRoot->data)<0) {
					if(currRoot->left==NULL) { 
						found = false; 
						searching = false;

					} else { currRoot=currRoot->left; }

				} else {
					if(currRoot->right==NULL) { 
						found = false; 
						searching = false;

					} else { currRoot=currRoot->right; }
				}
	
			} //end search loop
		}
	}

	return found;

} //end function


//Non-Recursive Height Update
void BST::UpdateHeight(Leaf* root) { 
	int counter = 1;
	this->UpdateHeight(root, counter);

} //end function


//Recursive Height Update
void BST::UpdateHeight(Leaf* root, int& currHeight) {
	
	root->leafHeight = currHeight;
	bool rotated = false;

	//If Has Kids
	if(root->left!=NULL) {
		root->leftHeight = root->left->leafHeight;
	}
	if(root->right!=NULL) {
		root->rightHeight = root->right->leafHeight;
	}
		
	//Balance
	if((root->leftHeight-root->rightHeight)>1) {
		//Found Imbalance - Left Heavy
		this->rotateRight(root);
		rotated = true;
		
	} else if((root->rightHeight-root->leftHeight)>1) {
		//Found Imbalance - Right Heavy
		this->rotateLeft(root);
		rotated = true;
	}

	//If Has Not Rotated Yet
	if(!rotated) {
		if(root->parent!=NULL) { //<--Has Parent
			currHeight++; //<--Traverse Up
			this->UpdateHeight(root->parent, currHeight);
	
		} 

	} 
	
} //end function


//Searches Tree For Opening & Inserts in Empty Child 
void BST::InsertLeaf(char* newData) {
	
	Leaf* currRoot = this->treeRoot;

	//If Empty Branch or Tree
	if(!currRoot) {
		this->treeRoot = this->MakeLeaf(newData, NULL);
	
	} else {
	
		bool inserting = true;

		//If Word Exists
		if(strcmp(newData, currRoot->data)==0) { inserting=false; }

		//While Not Flagged To Stop
		while(inserting) {
			
			if(strcmp(newData, currRoot->data)<0) {
				if(currRoot->left==NULL) { 
					currRoot->left = this->MakeLeaf(newData, currRoot);
					this->UpdateHeight(currRoot->left);
					inserting = false; 

				} else { currRoot=currRoot->left; }

			} else {
				if(currRoot->right==NULL) { 
					currRoot->right = this->MakeLeaf(newData, currRoot);
					this->UpdateHeight(currRoot->right);
					inserting = false;

				} else { currRoot=currRoot->right; }
			}

		} //end search loop
	
	}
	
} //end function


//Rotates Given Root Right
void BST::rotateRight(Leaf* currLeaf) {

	bool changedTreeRoot = (currLeaf == this->treeRoot); //<--Trigger Ptr Change          
	if (changedTreeRoot) { this->treeRoot = currLeaf->left; }
    
	//Store & Swap Leaf
	Leaf* rightRootCrossLeaf = currLeaf->left->right; 
	currLeaf->left->right = currLeaf;

	//Swap Parents
	currLeaf->left->parent = currLeaf->parent;
	currLeaf->parent = currLeaf->left;
	
	//If Has Node, Point To Parent
	//Adjust Heights
	if (rightRootCrossLeaf) { 
		rightRootCrossLeaf->parent = currLeaf; 
		currLeaf->leafHeight = rightRootCrossLeaf->leafHeight+1;
		currLeaf->leftHeight = rightRootCrossLeaf->leafHeight;

	} else {
		currLeaf->leafHeight = 1;
		currLeaf->leftHeight = 0;
	}

	//Change Left
    currLeaf->left = rightRootCrossLeaf;
    
	//If Not TreeRoot Inform Grandparents
    if(!changedTreeRoot) {
        if (currLeaf->parent->parent->right == currLeaf) {
            currLeaf->parent->parent->right = currLeaf->parent;
        } else {
            currLeaf->parent->parent->left = currLeaf->parent;
        }
    }

} //end function


//Rotates Given Root Left
void BST::rotateLeft(Leaf* currLeaf) {

	bool changedTreeRoot = (currLeaf == this->treeRoot); //<--Trigger Ptr Change          
	if (changedTreeRoot) { this->treeRoot = currLeaf->right; }
    
	//Store & Swap Leaf
	Leaf* leftRootCrossLeaf = currLeaf->right->left; 
	currLeaf->right->left = currLeaf;

	//Swap Parents
	currLeaf->right->parent = currLeaf->parent;
	currLeaf->parent = currLeaf->right;
	
	//If Has Node, Point To Parent
	//Adjust Heights
	if (leftRootCrossLeaf) { 
		leftRootCrossLeaf->parent = currLeaf; 
		currLeaf->leafHeight = leftRootCrossLeaf->leafHeight+1;
		currLeaf->rightHeight = leftRootCrossLeaf->leafHeight;

	} else {
		currLeaf->leafHeight = 1;
		currLeaf->rightHeight = 0;
	}

	//Change Right
    currLeaf->right = leftRootCrossLeaf;
    
	//If Not TreeRoot Inform Grandparents
    if(!changedTreeRoot) {
        if (currLeaf->parent->parent->left == currLeaf) {
            currLeaf->parent->parent->left = currLeaf->parent;
        } else {
            currLeaf->parent->parent->right = currLeaf->parent;
        }
    }


} //end function

//==============================================================