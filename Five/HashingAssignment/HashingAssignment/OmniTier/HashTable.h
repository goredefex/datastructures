#ifndef HASH_H
#define HASH_H

#include <string>
#include <iostream>
#include "BST.h"

class HashTable {

	//Table Size
	const int MAX_SIZE;
	
	//Letter Node/Indicies
	enum letter {
		a, b, c, d, e, f, g, h, i, j, k, l,
		m, n, o, p, q, r, s, t, u, v, w, x, 
		y, z
	};

	//HashNode Structure
	typedef struct HashNode {
		int key;
		char* value;
		BST* nodeTree;
	};
		
	//Private Members
	private:
		HashNode** table;

		//Inside Methods
		void deleteTableArray();
		HashNode* MakeNode(letter currChar);
		

	//Public Methods
	public:
		//Constructs
		HashTable(void);
		HashTable(int size);
		~HashTable(void);

		//Hash Functionality
		int GetIndex(char* newData);
		void put(char* value);
		bool searchTable(char* newData);
		void printTable();

		friend ostream& operator<<(ostream& inOs, HashTable& hT);


}; //EOC

#endif