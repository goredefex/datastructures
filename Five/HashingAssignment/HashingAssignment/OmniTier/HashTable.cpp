#include "stdafx.h"
#include "HashTable.h"


//==============================================================
//Constructs ---------------------------------------------------
//==============================================================

//Constructor - Default
HashTable::HashTable(void):MAX_SIZE(26) {
	
	//Instantite Hash Arrays
	this->table = new HashNode*[MAX_SIZE];
		
	for(int i=0; i<MAX_SIZE; i++) { this->table[i] = this->MakeNode(static_cast<letter>(i)); }
	
} //end constructor

//Constructor - Overload Takes Size
HashTable::HashTable(int size):MAX_SIZE(size) {
	
	//Instantite Hash Arrays
	this->table = new HashNode*[MAX_SIZE];
		
	for(int i=0; i<MAX_SIZE; i++) { this->table[i] = this->MakeNode(static_cast<letter>(i)); }
	
} //end constructor

//Destructor
HashTable::~HashTable(void) { this->deleteTableArray(); } //end destructor

//==============================================================




//==============================================================
//Functions ----------------------------------------------------
//==============================================================

//Find & Delete All Member Table Arrays
void HashTable::deleteTableArray() { 
	//Delete & Safe Pointers
	if(this->table!=NULL) {
		delete(this->table);
		this->table = NULL;
	}

} //end function


//Makes A New Node Unlinked
HashTable::HashNode* HashTable::MakeNode(letter currChar) {
	HashNode* temp = new HashNode();
	temp->nodeTree = new BST; //<--Make Tree
	temp->key = (int&)currChar;
	temp->value = NULL;
		
	return temp;

} //end function


//Uses First Letter To Reveal Index
int HashTable::GetIndex(char* newData) {
	switch (tolower(newData[0])) {
		case 'a':
			return letter::a;
		case 'b':
			return letter::b;
		case 'c':
			return letter::c;
		case 'd':
			return letter::d;
		case 'e':
			return letter::e;
		case 'f':
			return letter::f;
		case 'g':
			return letter::g;
		case 'h':
			return letter::h;
		case 'i':
			return letter::i;
		case 'j':
			return letter::j;
		case 'k':
			return letter::k;
		case 'l':
			return letter::l;
		case 'm':
			return letter::m;
		case 'n':
			return letter::n;
		case 'o':
			return letter::o;
		case 'p':
			return letter::p;
		case 'q':
			return letter::q;
		case 'r':
			return letter::r;
		case 's':
			return letter::s;
		case 't':
			return letter::t;
		case 'u':
			return letter::u;
		case 'v':
			return letter::v;
		case 'w':
			return letter::w;
		case 'x':
			return letter::x;
		case 'y':
			return letter::y;
		case 'z':
			return letter::z; }

} //end function


//Enters A Hash Node of newData Into Index of Key
void HashTable::put(char* newData) {
	int hash = this->GetIndex(newData);//<--Grab Index

	//Build Collision Handling
	if(this->table[hash]->value==NULL) { this->table[hash]->value = newData;
	} else {
		if(strcmp(this->table[hash]->value, newData)!=0) {
			if(!this->table[hash]->nodeTree->isInTree(newData))
				this->table[hash]->nodeTree->InsertLeaf(newData); 
			}
	}
	

} //end function


//Searches Table & Collision Tree For Word
bool HashTable::searchTable(char* newData) {

	int index = this->GetIndex(newData);
	bool found = false;

	//Is Index Value Correct Value
	if(strcmp(this->table[index]->value, newData)==0) { found = true; } 
	//If Not, Search Tree
	else { found = this->table[index]->nodeTree->isInTree(newData); }

	return found;

} //end function


//Prints All Contents Of Table
void HashTable::printTable() {
	for(int i=0; i<MAX_SIZE; i++) {
		cout << "-------------------" << endl;
		cout << "Bucket Key: " << this->table[i]->key << endl;
		cout << "Value: ";

		if(this->table[i]->value!=NULL) {
			cout << this->table[i]->value << endl;
			cout << "Collision Tree: " << endl; 
			cout << endl;
			this->table[i]->nodeTree->printTree();

		} else { cout << "Empty" << endl; }

		cout << "-------------------" << endl;
		cout << endl; cout << endl;
	}

} //end function

//==============================================================


//Friend ------------
ostream& operator<<(ostream& inOs, HashTable& hT) {
	hT.printTable();
	return inOs;

} //end function